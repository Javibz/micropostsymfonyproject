<?php

namespace App\Entity;

use App\Entity\Notification;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\LikeNotificationRepository")
 */
class LikeNotification extends Notification
{   
    /**
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\MicroPost")
     */
    private $microPost;

    /**
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\User") 
     */
    private $likedBy;
  
    /**
     * Get the value of likedBy
     */ 
    public function getLikedBy()
    {
        return $this->likedBy;
    }

    /**
     * Set the value of likedBy
     *
     * @return  self
     */ 
    public function setLikedBy($likedBy)
    {
        $this->likedBy = $likedBy;

        return $this;
    }

    /**
     * Get the value of microPost
     */ 
    public function getMicroPost()
    {
        return $this->microPost;
    }

    /**
     * Set the value of microPost
     *
     * @return  self
     */ 
    public function setMicroPost($microPost)
    {
        $this->microPost = $microPost;

        return $this;
    }
}
