<?php

namespace App\Event;

use App\Entity\User;
use Symfony\Component\EventDispatcher\Event;

class UserRegisterEvent extends Event
{
  const NAME = 'user.register';
  /**
   *
   * @var User
   */
  private $registeredUser;

  public function __construct(User $registeredUser)
  {
    $this->registeredUser = $registeredUser;
  }

  /**
   * Get the value of registeredUser
   *
   * @return  User
   */ 
  public function getRegisteredUser()
  {
    return $this->registeredUser;
  }
}