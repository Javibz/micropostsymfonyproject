<?php

namespace App\DataFixtures;

use App\Entity\User;
use App\Entity\MicroPost;
use App\Entity\UserPreferences;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AppFixtures extends Fixture
{
    private const USERS = [
        [
            'username' => 'john_doe',
            'email' => 'john_doe@doe.com',
            'password' => 'password',
            'fullName' => 'John Doe',
            'roles' => [User::ROLE_USER]
        ],
        [
            'username' => 'rob_smith',
            'email' => 'rob_smith@smith.com',
            'password' => 'password',
            'fullName' => 'Rob Smith',
            'roles' => [User::ROLE_USER]
        ],
        [
            'username' => 'marry_gold',
            'email' => 'marry_gold@gold.com',
            'password' => 'password',
            'fullName' => 'Marry Gold',
            'roles' => [User::ROLE_USER]
        ],
        [
            'username' => 'javi_ramon',
            'email' => 'john_ramon@email.com',
            'password' => 'password',
            'fullName' => 'Javi Ramon',
            'roles' => [User::ROLE_ADMIN]
        ],
    ];

    private const POST_TEXT = [
        'Hello, how are you?',
        'It\'s nice sunny weather today',
        'I need to buy some ice cream!',
        'I wanna buy a new car',
        'There\'s a problem with my phone',
        'I need to go to the doctor',
        'What are you up to today?',
        'Did you watch the game yesterday?',
        'How was your day?'
    ];

    private const LANGUAGES = ['en', 'fr'];

    /**
     * @var UserPasswordEncoderInterface
     */
    private $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder) {
        $this->encoder = $encoder;    
    }

    public function load(ObjectManager $manager)
    {
        // $faker = Factory::create('fr-FR');
        // $genres = ['male', 'female'];
        
        // $genre = $faker->randomElement($genres);
        // $picture = 'https://randomuser.me/api/portraits/';
        // $pictureId = $faker->numberBetween(1, 99) . '.jpg';
        
        // $picture .= ($genre == 'male' ? 'men/' : 'women/') . $pictureId;
        
        $this->loadUsers($manager);
        $this->loadMicroPosts($manager);
    }

    private function loadMicroPosts(ObjectManager $manager){
        for ($i=0; $i < 30; $i++) { 
            $microPost = new MicroPost();
            $date = new \DateTime();
            $date->modify('-' . rand(0, 20) . ' day');
            $microPost->setText(self::POST_TEXT[rand(0, count(self::POST_TEXT) - 1)])
                      ->setTime($date)
                      ->setUser($this->getReference(self::USERS[rand(0, count(self::USERS) - 1)]['username']));

            $manager->persist($microPost);
        }

        $manager->flush();
    }

    private function loadUsers(ObjectManager $manager){

        foreach(self::USERS as $userData){

            $user = new User();
            
            $user->setUsername($userData['username'])
            ->setFullName($userData['fullName'])
            ->setEmail($userData['email'])
            ->setPassword($this->encoder->encodePassword($user, $userData['password']))
            ->setRoles($userData['roles'])
            ->setEnabled(true);
            
            $this->addReference($userData['username'], $user);
            
            $preferences = new UserPreferences();
            $preferences->setLocale(self::LANGUAGES[rand(0, 1)]);
            
            $user->setPreferences($preferences);
    
            $manager->persist($user);
        }

        $manager->flush();
    }
}

