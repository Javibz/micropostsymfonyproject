<?php

namespace App\Controller;

use App\Repository\UserRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class SecurityController extends AbstractController
{
    /**
     * @Route("/login", name="security_login")
     */
    public function login(AuthenticationUtils $utils)
    {
        $error = $utils->getLastAuthenticationError();
        $username = $utils->getLastUsername();

        return $this->render('security/login.html.twig', [
            'username' => $username,
            'hasError' => $error
        ]);
    }

    /**
     * Allow to user's logout
     * 
     * @Route("/logout", name="security_logout")
     *
     */
    public function logout(){

    }

    /**
     * @Route("/confirm/{token}", name="security_confirm")
     * @return void
     */
    public function confirm(string $token, UserRepository $repo, ObjectManager $manager)
    {
        $user = $repo->findOneBy([
            'confirmationToken' => $token
        ]);

        if($user !== null){
            $user->setEnabled(true);
            $user->setConfirmationToken('');

            $manager->flush();
        }

        return $this->render('security/confirmation.html.twig', [
            'user' => $user,
        ]);
    }
}
