<?php

namespace App\Controller;

use App\Entity\Notification;
use App\Repository\NotificationRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Security("is_granted('ROLE_USER')")
 * @Route("/notification")
 */
class NotificationController extends AbstractController
{
    /**
     * @Route("/unread-count", name="notification_unread")
     */
    public function unreadCount(NotificationRepository $repoNotif)
    {
        return new JsonResponse([
            'count' => $repoNotif->findUnseenByUser($this->getUser())
        ]);
       
    }

    /**
     *
     * @Route("/all", name="notification_all")
     */
    public function notifications(NotificationRepository $repoNotif)
    {
        return $this->render('notification/notifications.html.twig', [
            'notifications' => $repoNotif->findBy([
                'seen' => false,
                'user' => $this->getUser()
            ])
        ]);
    }

    /**
     * @Route("/acknowledge/{id}", name="notification_acknowledge")
     * @param ObjectManager $manager
     * @param Notification $notification
     * @return void
     */
    public function acknowledge(ObjectManager $manager, Notification $notification)
    {  
        $notification->setSeen(true);
        $manager->flush();

        return $this->redirectToRoute('notification_all');
    }
    /**
     * @Route("acknowledge-all", name="notification_acknowledge_all")
     * @param NotificationRepository $repoNotif
     * @param ObjectManager $manager
     * @return void
     */
    public function acknowledgeAll(NotificationRepository $repoNotif,ObjectManager $manager)
    {
        $repoNotif->markAllAsReadByUser($this->getUser());
        $manager->flush();
        
        return $this->redirectToRoute('notification_all');
    }   
}
