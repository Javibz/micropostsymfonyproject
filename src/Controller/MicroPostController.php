<?php

namespace App\Controller;

use App\Entity\User;
use App\Entity\MicroPost;
use App\Form\MicroPostType;
use App\Repository\UserRepository;
use App\Repository\MicroPostRepository;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class MicroPostController extends AbstractController
{
    /**
     * @Route("/micro-post", name="micro_post_index")
     */
    public function index(MicroPostRepository $repo, UserRepository $userRepo, TokenStorageInterface $token)
    {
        $currentUser = $token->getToken()->getUser();

        $usersToFollow = [];

        if($currentUser instanceof User){
            $posts= $repo->findAllByUsers($currentUser->getFollowing());
            $usersToFollow = count($posts) === 0 ? $userRepo->findAllWithMoreThan5PostsExceptCurrentUser($currentUser) : [];
        } else {
            $posts = $repo->findBy([], ['time' => 'DESC']);

        }

        return $this->render('micro_post/index.html.twig', [
            'posts' => $posts,
            'usersToFollow' => $usersToFollow
        ]);
    }

    /**
     * Edit an existant post
     *
     * @Route("micro-post/edit/post-{id}", name="micro_post_edit")
     * @Security("is_granted('edit', microPost)", message="Access denied")
     * 
     * @return Response
     */
    public function edit(MicroPost $microPost, Request $request, ObjectManager $manager){
        $form = $this->createForm(MicroPostType::class, $microPost);
        $form->handleRequest($request);
        
        if($form->isSubmitted() && $form->isValid()){
            $manager->persist($microPost);
            $manager->flush();

            $this->addFlash(
                'success',
                "Le post a bien été modifié !"
            );
            
            return $this->redirectToRoute('micro_post_index');
        }
        
        return $this->render('micro_post/new.html.twig', 
        ['form' => $form->createView()]);
    }

    
    /**
     * Add a new microPost
     *
     * @Route("micro-post/add", name="micro_post_add")
     * @Security("is_granted('ROLE_USER')", message="Access denied")
     * @return Response
     */
    public function add(Request $request, ObjectManager $manager) {

        $user = $this->getUser();
        $microPost = new MicroPost();
        // $microPost->setTime(new \DateTime());
        $microPost->setUser($user);
        
        $form = $this->createForm(MicroPostType::class, $microPost);
        $form->handleRequest($request);
        
        if($form->isSubmitted() && $form->isValid()){
            $manager->persist($microPost);
            $manager->flush();
            
            $this->addFlash(
                'success',
                "Le post a bien été créé !"
            );

            return $this->redirectToRoute('micro_post_index');
        }
        
        return $this->render('micro_post/new.html.twig', 
        ['form' => $form->createView()]);
    }

    /**
     * Show one post
     *
     * @Route("micro-post/post-{id}", name="micro_post_post")
     * 
     * @return Response
     */
    public function post(MicroPostRepository $repo, MicroPost $microPost){
        
        return $this->render('micro_post/post.html.twig', [
            'post' => $microPost
        ]);
    }

    /**
     * Delete a micropost
     * @Route("micro-post/delete/post-{id}", name="micro_post_delete")
     * @Security("is_granted('delete', microPost)", message="Access denied") 
     */
    public function delete(MicroPost $microPost, ObjectManager $manager){
        $manager->remove($microPost);
        $manager->flush();

        $this->addFlash(
            'danger',
            "Le post a bien été supprimé !"
        );

        return $this->redirectToRoute('micro_post_index');

    }

    /**
     * Display only the user's posts
     * 
     * @Route("/user/{username}", name="micro_post_user")
     *
     */
    public function userPosts(MicroPostRepository $repo, User $user){
        // $posts = $repo->findBy(['user' => $user], ['time' => 'DESC']);
        $posts = $user->getPosts();

        return $this->render('micro_post/user-posts.html.twig', [
            'posts' => $posts,
            'user' => $user,
        ]);
    }
}
