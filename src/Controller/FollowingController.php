<?php

namespace App\Controller;

use App\Entity\User;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Security("is_granted('ROLE_USER')")
 * @Route("/following")
 */
class FollowingController extends AbstractController
{
    /**
     * @Route("/follow/{id}", name="following_follow")
     * 
     * @return Response
     */
    public function follow(User $userToFollow, ObjectManager $manager)
    {
        $currentUser = $this->getUser();
        if($userToFollow->getId() !== $currentUser->getId()){
            $currentUser->follow($userToFollow);
            
            $manager->flush();
        }

        return $this->redirectToRoute('micro_post_user', ['username' =>$userToFollow->getUsername()]);

    }

    /**
     * @Route("/unfollow/{id}", name="following_unfollow")
     */
    public function unfollow(User $userToUnfollow, ObjectManager $manager)
    {
        $currentUser = $this->getUser();
        $currentUser->getFollowing()->removeElement($userToUnfollow);

        $manager->flush();

        return $this->redirectToRoute('micro_post_user', ['username' =>$userToUnfollow->getUsername()]);
    }
}
