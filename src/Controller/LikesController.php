<?php

namespace App\Controller;

use App\Entity\User;
use App\Entity\MicroPost;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/likes")
 */
class LikesController extends AbstractController
{
    /**
     * @Route("/like/{id}", name="likes_like")
     */
    public function like(MicroPost $post, ObjectManager $manager)
    {
        /** @var User $currentUser */
        $currentUser = $this->getUser();

        if(!$currentUser instanceof User) {
            return new JsonResponse([], Response::HTTP_UNAUTHORIZED);
        }

        $post->like($currentUser);
        
        $manager->persist($currentUser);
        $manager->flush();

        return new JsonResponse([
            'count' => $post->getlikedBy()->count()
        ]);
    }

    /**
     * @Route("/unlike/{id}", name="likes_unlike")
     * @param MicroPost $post
     * @return void
     */
    public function unlike(MicroPost $post, ObjectManager $manager)
    {
        /** @var User $currentUser */
        $currentUser = $this->getUser();

        if(!$currentUser instanceof User) {
            return new JsonResponse([], Response::HTTP_UNAUTHORIZED);
        }

        $post->getlikedBy()->removeElement($currentUser);
        
        $manager->persist($currentUser);
        $manager->flush();

        return new JsonResponse([
            'count' => $post->getlikedBy()->count()
        ]);
    }
}